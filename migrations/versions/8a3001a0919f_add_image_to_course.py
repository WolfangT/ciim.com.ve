"""add image to course

Revision ID: 8a3001a0919f
Revises: b4a78e8aaec5
Create Date: 2020-06-25 23:55:05.074820

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8a3001a0919f'
down_revision = 'b4a78e8aaec5'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('courses', schema=None) as batch_op:
        batch_op.add_column(sa.Column('image', sa.BLOB(length=1048576), nullable=True))
        batch_op.add_column(sa.Column('mimetype', sa.String(length=20), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('courses', schema=None) as batch_op:
        batch_op.drop_column('mimetype')
        batch_op.drop_column('image')

    # ### end Alembic commands ###
