from setuptools import setup

setup(
    name="ciim.com.ve",
    version="0.1",
    description="CIIM page",
    author="Marcos Fuenmayor and Wolfang Torres",
    author_email="wolfangt@gmail.com",
    packages=["ciim"],
    package_data={"ciim": ["static/*", "templates/*"]},
    install_requires=[
        "flask",
        "flask_sqlalchemy",
        "flask_babelex",
        "flask_mail",
        "flask_login",
        "flask_admin",
        "flask_migrate",
        "flask_security",
        "email_validator",
        "markdown",
        "pyembed_markdown",
    ],
    scripts=["scripts/ciim"],
)
