"""security.py

Module for authentications fuctions
"""

import re
import random
import string

from flask_security import Security, SQLAlchemyUserDatastore
from flask_security.utils import encrypt_password

from . import mail
from .database import Roles, Users, Inscriptions, db

user_datastore = SQLAlchemyUserDatastore(db, Users, Roles)

RGX_EMAIL = re.compile(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$")
RGX_ID = re.compile(r"^\d{1,8}$")
RGX_CELL = re.compile(r"(^\+\d{8,}$)|(^\d{10,11}$)")


def init_app(app):
    """Init the module"""
    Security(app, user_datastore)


class SecurityError(Exception):
    """Base class for Security errors"""


class AuthError(SecurityError):
    """Main class for Authentication exceptions"""

    def __init__(self, *errors):
        self.errors = errors


def validate(regex, expression):
    """Simple regex checker"""
    return bool(regex.fullmatch(expression))


def generate_password():
    longofpass = random.randint(8, 13)
    password = ""
    for x in range(longofpass):
        password += random.choice(string.ascii_letters)
    return password


def create_user_from_inscription(newuser):
    password = generate_password()
    user = user_datastore.create_user(
        name=newuser.name,
        id_card=newuser.id_card,
        cellphone=newuser.cellphone,
        career=newuser.career,
        description=newuser.description,
        email=newuser.email,
        password=encrypt_password(password),
    )
    role = user_datastore.find_role("member")
    user_datastore.add_role_to_user(user, role)
    return password


def register_user(form):
    errors = []
    card = form["cedula"]
    if "." in form["cedula"]:
        card = card.replace(".","")
    if not validate(RGX_EMAIL, form["email"]):
        errors.append("Email incorrecto")
    if not validate(RGX_ID, card):
        errors.append("Cedula incorrecta")
    if not validate(RGX_CELL, form["cellphone"]):
        errors.append("Telefono incorrecto")
    if Inscriptions.query.filter_by(email=form["email"]).first():
        errors.append("Inscripcion ya existente")
    if Users.query.filter_by(email=form["email"]).first():
        errors.append("Usuario ya existente")
    if errors:
        raise AuthError(*errors)
    newmember = Inscriptions(
        name=form["realname"],
        id_card=card,
        cellphone=form["cellphone"],
        email=form["email"],
        career=form["especialidad"],
        description=form["descripcion"],
    )
    db.session.add(newmember)
    db.session.commit()
    mail.send_registration(form)


def edit_user(form, id_):
    if not validate(RGX_CELL, form.get("cellphone")):
        raise AuthError("Telefono Invalido")
    user = Users.query.filter_by(id=id_).first()
    user.career = form["career"]
    user.description = form["description"]
    user.cellphone = form["cellphone"]
    db.session.commit()
    mail.send_correct_edit_user(user.email)
