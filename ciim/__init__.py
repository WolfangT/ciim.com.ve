"""ciim

Webpage for te CIIM
Wolfang Torres - wolfang.torres@gmail.com
Marcos Fuenmayor - marcos.fuenmayorhtc@gmail.com
"""

from os import makedirs, getenv
from pathlib import Path

from flask_migrate import Migrate

from . import mail, admin, routes, database, security
from .server import CIIM

SRV_DIR = getenv("SRV_DIR", "/var/www")
INS_DIR = getenv("INS_DIR", "/var/lib")
LOG_DIR = getenv("LOG_DIR", "/var/log")


def create_conf(app):
    """Default configuration"""
    database_file = Path(app.instance_path, "database.sqlite").absolute()
    sender_email = "no-reply@ciim.com.ve"
    return {
        "SECRET_KEY": "secret-key-do-not-hack!!",
        "SQLALCHEMY_DATABASE_URI": f"sqlite:///{database_file}",
        "SQLALCHEMY_TRACK_MODIFICATIONS": False,
        # "MAIL_SERVER": "smtp.gmail.com",
        # "MAIL_USE_TLS": True,
        # "MAIL_USE_SSL": False,
        # "MAIL_PORT": 587,
        # "MAIL_USERNAME": "no.reply.ciim@gmail.com",
        "MAIL_DEFAULT_SENDER": sender_email,
        "SECURITY_PASSWORD_HASH": "sha512_crypt",
        "SECURITY_PASSWORD_SALT": "sha512_crypt",
        "SECURITY_POST_REGISTER_VIEW": "/login",
        "SECURITY_POST_CONFIRM_VIEW": "/login",
        "SECURITY_POST_RESET_VIEW": "/login",
        "SECURITY_CHANGEABLE": True,
        "SECURITY_EMAIL_SENDER": sender_email,
        "SECURITY_RECOVERABLE": True,
        "SECURITY_MSG_USER_DOES_NOT_EXIST": (
            "No existe una cuenta con este email",
            "error",
        ),
        "SECURITY_MSG_INVALID_PASSWORD": ("Contraseña inválida", "error"),
        "SECURITY_EMAIL_PLAINTEXT": False,
        "SECURITY_EMAIL_HTML": True,
        "SECURITY_EMAIL_SUBJECT_PASSWORD_RESET": "[CIIM] Solicitud de restablecimiento de contraseña",
        "SECURITY_EMAIL_SUBJECT_PASSWORD_CHANGE_NOTICE": "[CIIM] Su contraseña ha sido cambiada",
        "SECURITY_EMAIL_SUBJECT_PASSWORD_NOTICE": "[CIIM] Su contraseña ha sido restablecida",
        "SECURITY_MSG_PASSWORD_RESET": (
            "Restableciste tu contraseña y automaticamente has sido logeado",
            "success",
        ),
        "SECURITY_MSG_PASSWORD_RESET_EXPIRED": (
            "Ha expirado su solicitud de restablecimiento, se mandaron nuevas instrucciones a %(email)s",
            "error",
        ),
        "SECURITY_MSG_PASSWORD_RESET_REQUEST": (
            "Se mando instrucciones a %(email)s",
            "success",
        ),
        "SECURITY_MSG_PASSWORD_CHANGE": (
            "Su contraseña ha sido cambiada con éxito",
            "success",
        ),
    }


def create_app():
    """ciim.com.ve page server factory"""
    instance_path = (
        f"{INS_DIR}/ciim.com.ve" if INS_DIR else f"{SRV_DIR}/ciim.com.ve/instance"
    )
    print(f"Instance is on {instance_path}")
    app = CIIM(__name__, instance_path=instance_path)
    # make sure the instance folder exists
    try:
        makedirs(app.instance_path, exist_ok=True)
    except OSError:
        print(f"Access Error on instance path, {app.instance_path}")
    # load default conf
    app.config.from_mapping(create_conf(app))
    # load instance config file
    app.config.from_pyfile("config.py", silent=True)
    # initialise modules and plugin
    for module in (database, mail, admin, security):
        module.init_app(app)
    for blueprints in (routes,):
        app.register_blueprint(blueprints.bp)
    Migrate(app, database.db, render_as_batch=True)
    return app
