/*
    Directive by HTML5 UP
    html5up.net | @n33co
    Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
*/

(function($) {

    "use strict";

    skel.breakpoints({
        wide: '(max-width: 1680px)',
        normal: '(max-width: 1280px)',
        narrow: '(max-width: 980px)',
        narrower: '(max-width: 840px)',
        mobile: '(max-width: 736px)',
        mobilep: '(max-width: 480px)'
    });

    var imagenes = [
        "2.jpg",
        "4.jpg",
        "a1.jpg",
        "5.jpg",
        "6.jpg",
        "a2.jpg",
        "7.jpg",
        "8.jpg",
        "a4.jpg",
        "9.jpg",
        "10.jpg",
        "11.jpg",
        "a9.jpg",
    ];

    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }

    $(function() {

        var $window = $(window),
            $body = $('body'),
            $header = $('#header'),
            $banner = $('#banner');

        // Disable animations/transitions until the page has loaded.
            $body.addClass('is-loading');

            $window.on('load', function() {
                $body.removeClass('is-loading');
            });

        // Fix: Placeholder polyfill.
            $('form').placeholder();

        // Prioritize "important" elements on narrower.
            skel.on('+narrower -narrower', function() {
                $.prioritize(
                    '.important\\28 narrower\\29',
                    skel.breakpoint('narrower').active
                );
            });
        // wrapper.
        shuffle(imagenes);
        var fondoActual = 0;
        //~ cambiarFondo();
        //~ window.onload = function() {
            //~ setInterval(cambiarFondo, 60000);// Cada minuto
        //~ }
        function cambiarFondo() {
            var url = "url(images/backgrounds/" + imagenes[++fondoActual % imagenes.length] + ")"
            console.log(url);
            document.getElementById('page-wrapper').style.backgroundImage = url;
        }

        // Header.
            if (skel.vars.IEVersion < 9)
                $header.removeClass('alt');

            if ($banner.length > 0
            &&  $header.hasClass('alt')) {

                $window.on('resize', function() { $window.trigger('scroll'); });

                $banner.scrollex({
                    //~ top:        ($banner.outerHeight() + 200),
                    bottom:     $banner.outerHeight()/2,
                    terminate:  function() { $header.removeClass('alt'); },
                    enter:      function() { $header.addClass('alt'); },
                    leave:      function() { $header.removeClass('alt'); }
                });
            }

        // Menu.
            var $menu = $('#menu');

            $menu._locked = false;

            $menu._lock = function() {

                if ($menu._locked)
                    return false;

                $menu._locked = true;

                window.setTimeout(function() {
                    $menu._locked = false;
                }, 350);

                return true;

            };

            $menu._show = function() {

                if ($menu._lock())
                    $body.addClass('is-menu-visible');

            };

            $menu._hide = function() {

                if ($menu._lock())
                    $body.removeClass('is-menu-visible');

            };

            $menu._toggle = function() {

                if ($menu._lock())
                    $body.toggleClass('is-menu-visible');

            };

            $menu
                .appendTo($body)
                .on('click', function(event) {

                    event.stopPropagation();

                    // Hide.
                        $menu._hide();

                })
                .find('.inner')
                    .on('click', '.close', function(event) {

                        event.preventDefault();
                        event.stopPropagation();
                        event.stopImmediatePropagation();

                        // Hide.
                            $menu._hide();

                    })
                    .on('click', function(event) {
                        event.stopPropagation();
                    })
                    .on('click', 'a', function(event) {

                        var href = $(this).attr('href');

                        event.preventDefault();
                        event.stopPropagation();

                        // Hide.
                            $menu._hide();

                        // Redirect.
                            window.setTimeout(function() {
                                window.location.href = href;
                            }, 350);

                    });

            $body
                .on('click', 'a[href="#menu"]', function(event) {

                    event.stopPropagation();
                    event.preventDefault();

                    // Toggle.
                        $menu._toggle();

                })
                .on('keydown', function(event) {

                    // Hide on escape.
                        if (event.keyCode == 27)
                            $menu._hide();

                });

    });

})(jQuery);
