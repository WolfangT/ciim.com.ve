"""routes.py

main mdoule for application routes
"""

import markdown, math, operator

from io import BytesIO

from pyembed.markdown import PyEmbedMarkdown

from flask import (
    Blueprint,
    flash,
    request,
    url_for,
    redirect,
    render_template,
    abort,
    current_app,
    send_file
)
from flask_login import logout_user, current_user, login_required

from .security import AuthError, edit_user, register_user
from .database import Posts, db, Users, Courses, CoursesUsers

bp = Blueprint("ciim", __name__)


@bp.route("/inscripciones", methods=["GET", "POST"])
def inscripciones():
    if request.method == "POST":
        try:
            register_user(request.form)
        except AuthError as err:
            for error in err.errors:
                flash(f"{error}", "error")
        else:
            flash("Solicitud mandada con éxito", "success")
    return render_template("ciim/inscripciones.html")


@bp.route("/")
@bp.route("/index")
def index():
    return render_template("ciim/index.html")


@bp.route("/reglamento")
def reglamento():
    return render_template("ciim/reglamento.html")


@bp.route("/feria")
def feria():
    return render_template("ciim/feria.html")


@bp.route("/perfil", methods=("GET", "POST"))
@login_required
def perfil():
    if request.method == "POST":
        try:
            edit_user(request.form)
        except AuthError as err:
            for error in err.errors:
                flash(f"{error}", "error")
        else:
            flash("Solicitud mandada con éxito", "success")
        return redirect(url_for("ciim.perfil"))
    return render_template(
        "ciim/perfil.html", iseditable=bool(request.args.get("edit"))
    )


@bp.route("/logout")
@login_required
def logout():
    logout_user(current_user)
    return redirect(url_for("ciim.index"))


@bp.route("/editor", methods=("GET", "POST"))
@login_required
def editor():
    if request.method == "POST":
        id = 0
        if "id" in request.form:
            id = int(request.form.get("id"))
        post = Posts.query.filter_by(id=id).first()
        if id != 0 and post:
            if post.user_id == current_user.id:
                if "title" and "description" in request.form:
                    post.title = request.form.get("title")
                    post.description = request.form.get("description")
                if "content" in request.form:
                    content = request.form.get("content")
                    html = markdown.markdown(content, extensions=[PyEmbedMarkdown()])
                    db.session.commit()
                    return render_template(
                        "ciim/editor.html",
                        edit=False,
                        html=html,
                        textmarkdown=content,
                        post=post,
                    )
                elif "editcontent" in request.form:
                    content = request.form.get("editcontent")
                    db.session.commit()
                    return render_template(
                        "ciim/editor.html", edit=True, textmarkdown=content, post=post
                    )
                elif "savecontent" in request.form:
                    content = request.form.get("savecontent")
                    post.markdown = content
                    db.session.commit()
                    flash("Tu post ha sido guardado correctamente", "success")
                    return render_template(
                        "ciim/editor.html", edit=True, textmarkdown=content, post=post
                    )
                else:
                    return abort(404)
            else:
                return abort(403)
        else:
            return abort(404)
    elif "id" in request.args:
        post = Posts.query.filter_by(id=int(request.args.get("id"))).first()
        if post:
            if post.user_id == current_user.id:
                return render_template(
                    "ciim/editor.html", edit=True, post=post, textmarkdown=post.markdown
                )
            else:
                return abort(403)
        else:
            return abort(404)
    else:
        return abort(404)


@bp.route("/misproyectos/")
@bp.route("/misproyectos/<int:page>")
@login_required
def myprojects(page=1):
    posts = Posts.query.filter_by(user_id=current_user.id)
    count = posts.count()
    if count < 1:
        posts = None
    else:
        posts = posts.paginate(1, 5)
        while page != posts.page:
            if posts.has_next:
                posts = posts.next(error_out=False)
            else:
                return abort(404)
    return render_template(
        "ciim/misproyectos.html", posts=posts
    )


@bp.route("/nuevoproyecto", methods=("GET", "POST"))
@login_required
def newproject():
    if request.method == "POST":
        if "title" and "description" in request.form:
            newpost = Posts(user_id=current_user.id)
            if len(request.form.get("title")) < 64:
                newpost.title = request.form.get("title")
                newpost.description = request.form.get("description")
                db.session.add(newpost)
                db.session.commit()
                flash("Post creado correctamente, ahora deberas editarlo", "success")
                return redirect(url_for("ciim.editor", id=newpost.id))
            else:
                flash("Titulo hasta 64 caracteres", "error")
    return render_template("ciim/nuevoproyecto.html")


@bp.route("/markdoc")
@login_required
def markdoc():
    return render_template("ciim/markdoc.html")


@bp.route("/cursos")
@bp.route("/cursos/<int:page>")
def courses(page=1):
    courses = Courses.query.filter_by(is_finalized=False).order_by(Courses.creation_time.desc())
    count = courses.count()
    if count < 1:
        courses = None
    else:
        courses = courses.paginate(1, 5)
        while page != courses.page:
            if courses.has_next:
                courses = courses.next(error_out=False)
            else:
                return abort(404)
    return render_template("ciim/cursos.html", courses=courses)

@bp.route("/miscursos")
@bp.route("/miscursos/<int:page>")
@login_required
def teachcourses(page=1):
    if current_user.has_role("teacher"):
        courses = Courses.query.filter_by(user_id=current_user.id).order_by(Courses.creation_time.desc())
        count = courses.count()
        if count < 1:
            courses = None
        else:
            courses = courses.paginate(1, 5)
            while page != courses.page:
                if courses.has_next:
                    courses = courses.next(error_out=False)
                else:
                    return abort(404)
        return render_template("ciim/miscursos.html", courses=courses)
    else:
        return abort(403)

@bp.route("/image")
def image():
    """ Public images """
    if "id" and "table" in request.args:
        if request.args.get("table") == "courses":
            course = Courses.query.filter_by(id=int(request.args.get("id"))).first()
            if course:
                if course.mimetype:
                    return send_file(BytesIO(course.image), mimetype=course.mimetype, cache_timeout=-1)
                else:
                    return abort(404)
            else:
                return abort(404)
        elif request.args.get("table") == "posts":
            pass
        else:
            return abort(404)
    else:
        return abort(404)

@bp.route("/nuevocurso", methods=("GET", "POST"))
@login_required
def newcourse():
    if current_user.has_role("teacher"):
        if request.method == "POST":
            if "title" and "description" in request.form:
                newcourse = Courses(user_id=current_user.id)
                if len(request.form.get("title")) < 64:
                    newcourse.title = request.form.get("title")
                    newcourse.description = request.form.get("description")
                    if request.files['image'].filename != '':
                        newcourse.image = request.files["image"].read()
                        newcourse.mimetype = request.files["image"].mimetype
                    db.session.add(newcourse)
                    db.session.commit()
                    flash("Curso creado correctamente", "success")
                    return redirect(url_for("ciim.teachcourses"))
                else:
                    flash("Titulo hasta 64 caracteres", "error")
        return render_template("ciim/nuevocurso.html")
    else:
        return abort(403)

@bp.route("/curso/aprobar/<int:id>", methods=("GET", "POST"))
@login_required
def approve(id):
    course = Courses.query.filter_by(id=id).first()
    if current_user.id == course.user_id:
        if "toggle" in request.args:
            user_id = int(request.args.get("toggle"))
            course_user = CoursesUsers.query.filter_by(user_id=user_id).first()
            course_user.approve = not course_user.approve
            db.session.commit()
        return render_template("ciim/aprobar.html", course=course)
    else:
        return abort(403)

@bp.route("/curso/<int:id>", methods=("GET", "POST"))
def course(id):
    course = Courses.query.filter_by(id=id).first()
    teacher = Users.query.filter_by(id=course.user_id).first()
    if "join" in request.args:
        if not course.is_student(current_user):
            flash("Solicitud mandada", "success")
            course.add_student(current_user)
            db.session.commit()
        else:
            flash("¡Ya mandaste la solicitud!", "error")
    is_student = course.is_student(current_user)
    is_approved = course.is_approved(current_user)
    if "delete" in request.args and current_user.id == course.user_id:
        if bool(request.args.get("delete")):
            db.session.delete(course)
            db.session.commit()
            flash("Curso borrado", "success")
            return redirect(url_for("ciim.teachcourses"))
    if request.method == "POST" and current_user.id == course.user_id:
        if "finish" in request.form:
            course.is_finalized = True
            flash("Curso finalizado", "success")
        if "start" in request.form:
            course.is_finalized = False
            flash("Curso reanudado", "success")
        if request.form['title'] != "":
            course.title = request.form['title']
            flash("Titulo cambiado", "success")
        if request.form['description'] != "":
            course.description = request.form['description']
            flash("Descripcion cambiada", "success")
        if request.files['image'].filename != '':
            course.image = request.files['image'].read()
            course.mimetype = request.files['image'].mimetype
            flash("Imagen cambiada", "success")
        db.session.commit()
    return render_template("ciim/curso.html", 
                            course=course, 
                            teacher=teacher, 
                            is_student=is_student,
                            is_approved=is_approved)


@bp.route("/proyecto/<int:id>")
def post(id):
    post = Posts.query.filter_by(id=id).first()
    if "delete" in request.args:
        if post.user_id == current_user.id:
            db.session.delete(post)
            db.session.commit()
            flash("Proyecto eliminado con éxito", "success")
            return redirect(url_for("ciim.myprojects"))
        else:
            return abort(403)
    if "like" in request.args:
        if not post.isliked(current_user):
            post.addlike(current_user)
            db.session.commit()
    if "unlike" in request.args:
        if post.isliked(current_user):
            post.unlike(current_user)
            db.session.commit()
    if post:
        author = Users.query.filter_by(id=post.user_id).first()
        html = None
        if post.markdown:
            html = markdown.markdown(post.markdown, extensions=[PyEmbedMarkdown()])
        return render_template(
            "ciim/proyecto.html", post=post, author=author, html=html
        )
    else:
        abort(404)


@bp.route("/proyectos")
@bp.route("/proyectos/<int:page>")
def posts(page=1):
    posts = Posts.query.order_by(Posts.likes_count.desc(), Posts.creation_time.desc())
    count = posts.count()
    if count < 1:
        posts = None
    else:
        posts = posts.paginate(1, 5)
        while page != posts.page:
            if posts.has_next:
                posts = posts.next(error_out=False)
            else:
                return abort(404)
    return render_template(
        "ciim/proyectos.html", posts=posts
    )
