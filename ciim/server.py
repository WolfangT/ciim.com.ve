"""server.py

module to store the main class fot the server and its tools
"""

from flask import Flask


class CIIM(Flask):
    """Main server class"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs, instance_relative_config=True)
