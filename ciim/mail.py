"""mail.py

Module for sending emails
"""

from flask import render_template, current_app
from flask_mail import Mail, Message

mail = Mail()


def init_app(app):
    mail.init_app(app)


def send_mail(destination, title, message):
    if not current_app.config["DEBUG"]:
        mail.send(
            Message(
                recipients=destination,
                subject="[CIIM] {}".format(title),
                html=render_template("mail/planilla.html", message=message),
            )
        )


def send_registration(form):
    """Send the user an email for their inscription"""
    message = (
        "Hemos recibido tu solicitud de inscripción, le solicitamos encarecidamente"
        " que espere por parte de los administradores, en breve se le hara saber"
        " el estado de su solicitud"
    )
    send_mail(
        [form["email"]], f"Hola {form['realname']}", message,
    )


def send_activate_inscription(email, password):
    message = (
        "Tu solicitud de inscripcion ha sido aprobada ¡Felicidades!"
        " puedes ingresar a tu cuenta usando tu direccion de correo"
        f" y esta contraseña: {password}"
    )
    send_mail([email], "Solicitud Aprobada", message)


def send_reject_inscription(email):
    message = (
        "Tu solicitud de inscripcion ha sido denegada"
        " ¡Intenta de nuevo la próxima vez!"
    )
    send_mail([email], "Solicitud Denegada", message)


def send_correct_edit_user(email):
    message = "Se han cambiado exitosamente los datos de tu usuario"
    send_mail([email], "Edicion de datos exitosa", message)
