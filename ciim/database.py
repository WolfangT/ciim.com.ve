"""database.py

Module for SQLAlchemy interface and the database models
"""

from datetime import datetime

import click
from flask.cli import with_appcontext
from flask_security import UserMixin, RoleMixin
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy(
    session_options={"autoflush": False, "autocommit": False, "expire_on_commit": False}
)


roles_users = db.Table(
    "roles_users",
    db.Column("user_id", db.Integer(), db.ForeignKey("users.id")),
    db.Column("role_id", db.Integer(), db.ForeignKey("roles.id")),
)

likes_users = db.Table(
    "likes_users",
    db.Column("user_id", db.Integer(), db.ForeignKey("users.id")),
    db.Column("post_id", db.Integer(), db.ForeignKey("posts.id")),
)

class CoursesUsers(db.Model):
    __tablename__ = "courses_users"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column("user_id", db.Integer(), db.ForeignKey("users.id"))
    course_id = db.Column("course_id", db.Integer(), db.ForeignKey("courses.id"))
    approve = db.Column("approve", db.Boolean(), default=False)
    __table_args__ = (db.UniqueConstraint('user_id', 'course_id', name='uniq_user_course'),)

class Inscriptions(db.Model):

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(50))
    id_card = db.Column(db.String(20))
    cellphone = db.Column(db.String(20))
    email = db.Column(db.String(50), unique=True)
    career = db.Column(db.String(50))
    description = db.Column(db.String(200))
    creation_time = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return f"<User {self.email}>"


class Roles(db.Model, RoleMixin):
    """Roles the users can have"""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    description = db.Column(db.String(255))

    def __str__(self):
        return f"{self.name} (id: {self.id})"

    def __repr__(self):
        return f"<Role ({self.id}) {self.name}>"


class Users(db.Model, UserMixin):
    """The users of the system"""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    email = db.Column(db.String(64), unique=True)
    password = db.Column(db.String(255), nullable=False)
    id_card = db.Column(db.String(20))
    cellphone = db.Column(db.String(20))
    career = db.Column(db.String(50))
    description = db.Column(db.String(200))
    creation_time = db.Column(db.DateTime, default=datetime.utcnow)
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship(
        Roles, secondary=roles_users, backref=db.backref("users", lazy="dynamic")
    )

    def __str__(self):
        return f"{self.name} (id: {self.id})"

    def __repr__(self):
        return f"<User ({self.id}) {self.name}>"




class Courses(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64), default="")
    description = db.Column(db.Text(), default="")
    is_finalized = db.Column(db.Boolean(), default=False)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    user = db.relationship(Users, backref="courses")
    creation_time = db.Column(db.DateTime, default=datetime.utcnow)
    students = db.relationship(
        Users, secondary="courses_users", backref=db.backref("users", lazy="dynamic")
    )
    image = db.Column(db.BLOB(1024*1024), nullable=True)
    mimetype = db.Column(db.String(20), nullable=True)

    @property
    def members(self):
        status_students = []
        for student in self.students:
            course_user = CoursesUsers.query.filter_by(user_id=student.id).first()
            student.approve = course_user.approve
            status_students.append(student)
        return status_students
    
    def add_student(self, User):
        self.students.append(User)
    
    def is_student(self, User):
        for student in self.students:
            if student == User:
                return True
        return False
    
    def is_approved(self, User):
        for student in self.students:
            if student == User:
                course = CoursesUsers.query.filter_by(user_id=student.id).first()
                return course.approve
        return None


class Lessons(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64), default="")
    description = db.Column(db.Text(), default="")
    markdown = db.Column(db.Text(4294000000), default="")
    creation_time = db.Column(db.DateTime, default=datetime.utcnow)
    course_id = db.Column(db.Integer, db.ForeignKey("courses.id"))
    course = db.relationship(Courses, backref="lessons")


class Posts(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64), default="")
    description = db.Column(db.Text(), default="")
    markdown = db.Column(db.Text(4294000000), default="")
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    creation_time = db.Column(db.DateTime, default=datetime.utcnow)
    likes_count = db.Column(db.Integer, default=0)
    user = db.relationship(Users, backref="posts")
    like = db.relationship(
       Users, secondary=likes_users, backref=db.backref("likes", lazy="dynamic"),
    )

    @property
    def likes(self):
        return len(self.like)

    def isliked(self, user):
        return bool(user in self.like)

    def addlike(self, user):
        if not self.isliked(user):
            self.like.append(user)
            self.likes_count = len(self.like)

    def unlike(self, user):
        if self.isliked(user):
            self.like.remove(user)
            self.likes_count = len(self.like)


def init_app(app):
    """Initializes the db object with the flask app"""
    db.init_app(app)
    app.cli.add_command(reset_db_command)
    app.cli.add_command(change_admin_password)


@click.command("master-pass")
@click.password_option()
@with_appcontext
def change_admin_password(password):
    """changes the admin password"""
    from .security import user_datastore as data
    from flask_security.utils import encrypt_password
    admin = data.get_user(1)
    if admin:
        admin.password = encrypt_password(password)
        data.put(admin)
        data.commit()
        click.echo("Admins password changed")
    else:
        click.echo("Oh ohh !!... No users")


@click.command("reset-db")
@with_appcontext
def reset_db_command():
    """Installs or purges the database (Danger!)"""
    from .security import user_datastore

    now = datetime.utcnow()
    db.drop_all()
    db.create_all()
    roles = [
        user_datastore.create_role(name="admin", description="System administrator"),
        user_datastore.create_role(name="member", description="Normal user"),
        user_datastore.create_role(name="teacher", description="Teacher of courses"),
    ]
    admin = user_datastore.create_user(
        name="admin",
        email="admin@ciim.com.ve",
        password="fa542e168c418d9c",
        active=True,
    )
    user_datastore.add_role_to_user(admin, roles[0])
    user_datastore.commit()
    click.echo("Initialized the database")
