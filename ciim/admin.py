from inspect import isclass

from flask import abort, request, url_for, redirect
from flask_admin import Admin, AdminIndexView, expose
from flask_login import current_user
from flask_admin.actions import action
from flask_admin.contrib.sqla import ModelView
from flask_admin.contrib.sqla.fields import QuerySelectMultipleField
from wtforms import Form, HiddenField, StringField, BooleanField, PasswordField
from wtforms.validators import Email, Required
from datetime import datetime


from . import mail, security
from .database import Inscriptions, db, Users, Roles
from flask_security.utils import encrypt_password


def init_app(app):
    """Inits the admin module"""
    with app.app_context():
        for name, cls in globals().items():
            if isclass(cls):
                if (
                    issubclass(cls, ModelView)
                    and hasattr(cls, "__model__")
                    and cls.__model__ is not None
                ):
                    admin.add_view(cls())
    admin.init_app(app)


class AdminRequired:
    """Custom model view with access restrictions"""

    def is_accessible(self):
        """Defines is accecible by the user"""
        return current_user.has_role("admin")

    def inaccessible_callback(self, name, **kwargs):
        """Defines what happens if the user can't access"""
        if not current_user.is_authenticated:
            return redirect(url_for("security.login", next=request.url))
        else:
            abort(403)


class HomeView(AdminRequired, AdminIndexView):
    """Viw for the admins's home page"""

    __name__ = "Home"

    @expose("/")
    def index(self):
        # TODO: make a better home admin page
        return redirect("inscriptions")


class ModelView(AdminRequired, ModelView):
    """Model View that requires admin access"""

    __model__ = None
    __category__ = None
    __view_name__ = None
    ModelViews = []

    def __init__(self, model=None, **kwargs):
        if self.__category__:
            kwargs["category"] = self.__category__
            kwargs["name"] = self.__view_name__
        if model:
            super().__init__(model, db.session, **kwargs)
        else:
            super().__init__(self.__model__, db.session, **kwargs)
        self.ModelViews.append(self)


class RecentEvents(ModelView):
    """Base classs for SQL Views that inform recent events"""

    __category__ = "Recent Events"
    can_edit = False
    can_delete = False
    can_create = False

    def activate(self, _id=None):
        """Aproves the record, to be defined by decendants"""

    def reject(self, _id=None):
        """Disapproves the record, to be defined by decendants"""

    @action(
        "approve", "Approve", "Are you sure you want to approve the selected users?"
    )
    def action_approve(self, ids):
        """Aproves multiple Users"""
        for _id in ids:
            redirect_value = self.activate(_id)
        return redirect_value

    @expose("/approve/", methods=["POST"])
    def approve(self):
        """Action for activating the record"""
        return self.activate()

    @action(
        "disapprove",
        "Disapprove",
        "Are you sure you want to disapprove the selected users?",
    )
    def action_disapprove(self, ids):
        """Disapprove multiple Users"""
        for _id in ids:
            redirect_value = self.reject(_id)
        return redirect_value

    @expose("/disapprove/", methods=["POST"])
    def disapprove(self):
        """Action for disapprove the inscription"""
        return self.reject()


class NewInscriptionsView(RecentEvents):
    """Custom view for the SQL view new_purchaces"""

    __model__ = Inscriptions

    def inscription_action(function):
        """Wrapper for Inscriptions actions"""

        def wrapper(self, id_):
            newuser = Inscriptions.query.filter_by(id=id_).first()
            function(self, newuser)
            db.session.delete(newuser)
            db.session.commit()

        return wrapper

    @inscription_action
    def reject(self, newuser):
        mail.send_reject_inscription(newuser.email)

    @inscription_action
    def activate(self, newuser):
        password = security.create_user_from_inscription(newuser)
        mail.send_activate_inscription(newuser.email, password)


class Members(ModelView):
    """Base classs for SQL Views Users"""

    __category__ = "Members"



class HashPasswordField(PasswordField):
    def process_formdata(self, valuelist):
        if valuelist:
            self.data = encrypt_password(valuelist[0])


class UserForm(Form):
    """Custom form for creating a user"""
    name = StringField("Nombres", [Required()])
    email = StringField("Email", [Email(), Required()])
    password = HashPasswordField("Password", [Required()])
    roles = QuerySelectMultipleField(
        "Roles", query_factory=lambda: Roles.query.all()
    )
    active = BooleanField("Active", default=True)


class Users(Members):
    """Custom view for the SQL view new_purchaces"""

    __model__ = Users

    column_list = (
        "name",
        "email",
        "id_card",
        "cellphone",
        "career",
        "description",
        "creation_time",
        "roles"
    )

    form_edit_rules = (
        "name",
        "email",
        "id_card",
        "cellphone",
        "career",
        "description",
        "creation_time",
        "roles"
    )
    
    def get_create_form(self):
        return UserForm






admin = Admin(name="CIIM Admin", template_mode="bootstrap3", index_view=HomeView())
