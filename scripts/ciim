#! /bin/sh
# General use script for building and updating gtomachine
# Wolfang Torres - wolfang.torres@gmail.com
# Marcos Fuenmayor - 

if [ $(id -u) != 0 ] ; then
    echo "$ProgName needs to be run as root"
    exit 1
fi

[ -z $SRV_DIR ] && SRV_DIR="/var/www"
[ -z $LOG_DIR ] && LOG_DIR="/var/log"
[ -z $INS_DIR ] && INS_DIR="/var/lib"
[ -z $CONF_DIR ] && CONF_DIR="/etc/apache2/sites-available"

ProgName=$(basename $0)
StartingDir=$(pwd)
Name="ciim.com.ve"
User="www-data"
Base="$SRV_DIR/$Name"
LogFile="$LOG_DIR/apache2/error.log"

help="Usage: $ProgName [OPTIONS] SUBCOMAND

  Comand line utility to Control de Gtomachine Server

  SUBCOMANDS:
    logs        --> Shows the logs
    restart     --> Restart the server
    update      --> Updates to the last version, configures and restarts
    config      --> Set up the system configuration

  OPTIONS:
    -h | --help --> prints this help message and exits
  "

sub_config() {
    mkdir -p "$INS_DIR/$Name" ; mkdir -p "$LOG_DIR/$Name"
    chown -R "$User:$User" "$Base" ; chmod -R ug+rwx "$Base"
    chown -R "$User:$User" "$INS_DIR/$Name" ; chmod -R ug+rwx "$INS_DIR/$Name"
    chown -R "$User:$User" "$LOG_DIR/$Name" ; chmod -R ug+rwx "$LOG_DIR/$Name"
    ln -fs "$Base/$Name.conf" "$CONF_DIR/$Name.conf"
    a2ensite "$Name"
    python3 -m flask db upgrade
}

sub_update() {
    echo "Updating, reconfiguring $Name"
    cd "$Base"
    git pull
    python3 -m pip install -U -e .
    cd "$StartingDir"
    sub_config
    service apache2 restart
}

sub_restart() {
    echo "Restarting $Name"
    service apache2 restart
    sub_logs
}

sub_logs() {
    if [ -z $1 ] ; then
        echo "==================== Reading Logs ======================"
        tail -n 2 -f "$LogFile"
    elif [ $1 = "-a" ] ; then
        less +G "$LogFile"
    else
        echo "Option "$1" not recognised, available:"
        echo "  -a | --all  -->  Shows all the log"
        exit 1
    fi
}

[ -z "$1" ] && echo "$help" && exit 0
while [ ! -z "$1" ]; do
    case $1 in
    "-h" | "--help" ) echo "$help" ; exit 0 ;;
    * )
         cmd="$1"
        shift
        sub_${cmd} $@
        if [ $? = 127 ]; then
            echo "Error: '$1' is not a known subcommand." >&2
            echo "       Run '$ProgName --help' for a list of known subcommands." >&2
            exit 1
        fi
        exit 0
        ;;
    esac
    shift
done
