import sys
import logging
from pathlib import Path

from ciim import create_app

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, str(Path(__file__).parent))


application = create_app()
