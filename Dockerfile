FROM wolfangtorres/base-apache2-flask
LABEL maintainer="Marcos Fuenmayor and Wolfang Torres"
LABEL version="0.1"
LABEL description="Webapge server for ciim.com.ve"

ENV FLASK_APP ciim
ENV FLASK_ENV production
ENV CIIM_URL ciim.com.ve

COPY . ${SRV_DIR}/${CIIM_URL}
COPY ./${CIIM_URL}.conf  ${CONF_DIR}/${CIIM_URL}.conf

WORKDIR ${SRV_DIR}/${CIIM_URL}
RUN python3 -m pip install -e . \
 && ciim config \ 
 && a2dissite 000-default
